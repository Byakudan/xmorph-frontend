import React, { Component } from 'react'
import './InfoPage.css'
import NavBarView from '../components/NavBarView'
import ReactPlayer from "react-player";
import image from '../assets/media/yarn.jpg'

export class InfoPage extends Component {
  render() {
    return (
      <div className="page">
        <NavBarView />
        <div className="page-header page-header1">
            And here's what you should know about us.
        </div>
        <div className="info-page">
            <div className="info-container">
                <img src={image} />
                <div className="row custom-row">
                    <div className="col col-left">
                        <div className="title">
                            Who?
                        </div>
                        <div>
                            We are a team of multidisciplinary designers and developers.  We create Brand Identities & digital experiences focusing on Human Interaction and Human-centered design.
                        </div>
                        <div className="title">
                            Where?
                        </div>
                        <div>
                            We live in the heart of start up capital of India: Bengaluru. We work remotely collaborating on Teams & Meets for clients across 3 time zones. 
                        </div>
                        <div className="title">
                            What?
                        </div>
                        <div>
                            We create brand identities, digital experiences, products, motion & communication design.
                        </div>
                    </div>
                    <div className="col col-right">
                        <div className="title">
                            If you have a potential project or would like to collaborate
                            please contact Abhishek at:
                        </div>
                        <div>
                            abhishekvastrad@gmail.com
                        </div>
                        <div className="title">
                            Call us
                        </div>
                        <div>
                            +91-9741451473
                        </div>
                        <div className="title">
                            Follow us
                        </div>
                        <div className="row custom-row">
                            <div className="social-link">
                                Youtube
                            </div>
                            <div className="social-link"> 
                                Instagram
                            </div>
                            <div className="social-link">
                                Linkedin
                            </div>
                        </div>
                        <div className="row custom-row social-link-row">
                            <div className="social-link">
                                Twitter
                            </div>
                            <div className="social-link">
                                Facebook
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="footer-name">© 2021 xmorph</div>

      </div>
    )
  }
}

export default InfoPage
