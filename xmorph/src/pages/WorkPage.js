import React, { Component } from 'react'
import './WorkPage.css'
import NavBarView from '../components/NavBarView'

export class WorkPage extends Component {
  render() {
    return (
      <div className="page">
        <NavBarView />
        <div className="page-header">
            Some of the works that we are proud to showcase
        </div>
        <div className="product-section prohedger-section">
            <div className="product-header">Prohedger</div>
            <div className="product-subtitle">Brand Identity</div>
        </div>
        <div className="row">
            <div className="product-section project2-section section-left">
                <div className="product-header">Prohedger</div>
                <div className="product-subtitle">Brand Identity</div>
            </div>
            <div className="product-section project3-section section-right">
                <div className="product-header">Prohedger</div>
                <div className="product-subtitle">Brand Identity</div>
            </div>
        </div>
        <div className="product-section prohedger-section">
            <div className="product-header">Prohedger</div>
            <div className="product-subtitle">Brand Identity</div>
        </div>
        <div className="row">
            <div className="product-section project2-section section-left">
                <div className="product-header">Prohedger</div>
                <div className="product-subtitle">Brand Identity</div>
            </div>
            <div className="product-section project3-section section-right">
                <div className="product-header">Prohedger</div>
                <div className="product-subtitle">Brand Identity</div>
            </div>
        </div>
        <div className="footer-name">© 2021 xmorph</div>
      </div>
    )
  }
}

export default WorkPage
