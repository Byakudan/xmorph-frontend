import React, { Component } from 'react'
import './HomePage.css'
import NavBarView from '../components/NavBarView'
import ReactPlayer from "react-player";

export class HomePage extends Component {
  render() {
    return (
      <div className="page">
        <NavBarView />
        <div className="container">
          <div className="background-layout">
            
            {/* <ReactPlayer
              url="https://takeyourtime.poltronafrau.com/static/media/story1.mp4?ss"
              playing={true}
              width="100%"
              height="auto"
              loop={true}
              controls={false}
            /> */}
          </div>
          <div className="foreground-layout">
            Designed with purpose.
          </div>
        </div>
        <div className="bottom-layout">
          <div className="bottom-header">Featured</div>
          <div className="product-section prohedger-section">
            <div className="product-header">Prohedger</div>
            <div className="product-subtitle">Brand Identity</div>
          </div>
          <div className="product-section project2-section">
            <div className="product-header">Prohedger</div>
            <div className="product-subtitle">Brand Identity</div>
          </div>
          <div className="product-section project3-section">
            <div className="product-header">Prohedger</div>
            <div className="product-subtitle">Brand Identity</div>
          </div>
          <div className="view-more" onClick={e => window.location = '\\work'}>view more</div>
          <div className="footer-name">© 2021 xmorph</div>
        </div>
      </div>
    )
  }
}

export default HomePage
