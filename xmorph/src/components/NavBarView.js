import React, { Component } from 'react'
// import './NavBarView.css'
import './NavBarView.css'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import menu from '../assets/media/menu.svg'

export class NavBarView extends Component {
  render() {
    return (
      <Router>
        <div>
          <div className="navbar">
            <div className="logo">
              <Link to="\" onClick={e => window.location = '\\'}>xmorph</Link>
            </div>
            <div className="work-option option" >
              <Link to="/work" onClick={e => window.location = '\\work'}>work</Link>
            </div>
            <div className="philosophy-option option">
              <Link to="/philosophy" onClick={e => window.location = '\\philosophy'}>philosophy</Link>
            </div>
            <div className="info-option option">
              <Link to="/info" onClick={e => window.location = '\\info'}>info</Link>
            </div>
          </div>
          <div className="mobile-navbar">
            <div className="logo mobile-logo">
              <Link to="\" onClick={e => window.location = '\\'}>xmorph</Link>
            </div>
            <div className="menu-container">
              <img src={menu}/>
            </div>
            <div className="mobile-options">
              <div onClick={e => window.location = '\\work'} className="mobile-option">work</div>
              <div onClick={e => window.location = '\\philosophy'} className="mobile-option">philosophy</div>
              <div onClick={e => window.location = '\\info'} className="mobile-option last-option">info</div>
            </div>
          </div>
        </div>

      </Router>

    )
  }
}

export default NavBarView
