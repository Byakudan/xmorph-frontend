import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import HomePage from './pages/HomePage'
import WorkPage from './pages/WorkPage'
import PhilosophyPage from './pages/PhilosophyPage';
import InfoPage from './pages/InfoPage';

ReactDOM.render(
  <Router>
    <div>
      <Switch>
        <Route exact path="/"  component={HomePage} />
        <Route exact path="/home" component={HomePage} />
        <Route exact path="/work" component={WorkPage}  />
        <Route exact path="/philosophy" component={PhilosophyPage}  />
        <Route exact path="/info" component={InfoPage}  />
      </Switch>
    </div>
  </Router>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
